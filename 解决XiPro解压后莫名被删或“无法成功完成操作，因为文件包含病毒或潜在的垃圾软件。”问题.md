# 解决XiPro解压后莫名被删或“无法成功完成操作，因为文件包含病毒或潜在的垃圾软件。”问题
>注：如出现如下情况即是被**Windows Defender**（电脑自带杀毒软件） 误杀

[![pFuaj5F.png](https://s11.ax1x.com/2024/01/29/pFuaj5F.png)](https://imgse.com/i/pFuaj5F)

>本教程示例XiPro版本为6.27.4
仅作为参考 如与教程不对请参考对应版本

>注：本教程遵守[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)许可证

## 一劳永逸的方法（关闭Windows Defender）

1.点击[这里](https://gitea.com/PeterJRyan0540/XiPro_Problem_Solving_Library/src/branch/main/Tools/Defender%20Control%20v2.1.zip)下载**Defender Control**用于关闭**Windows Defender**

2.先不解压，点击**Win**键，然后搜索**Windows 安全中心**，点击**病毒与威胁防护** 往下滑找到 **“病毒和威胁保护”设置**，然后点击下面的**管理设置**，将里面的所有选项关闭后再解压

[![pFKQSQU.png](https://s11.ax1x.com/2024/01/30/pFKQSQU.png)](https://imgse.com/i/pFKQSQU)


3.解压后打开**dControl.exe**应用，点击**停用 Windows Defender** ，当显示**Windows Defender已关闭**时，解压XiPro就不会出现文件被删的情况

[![pFKQAF1.png](https://s11.ax1x.com/2024/01/30/pFKQAF1.png)](https://imgse.com/i/pFKQAF1)