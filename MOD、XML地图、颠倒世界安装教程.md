# MOD、XML地图、颠倒世界安装教程
>注：本教程遵守[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)许可证

>本教程不包括线下MOD，线下MOD将会另开一期

>本教程示例版本为**XiPro 6.26.3** 仅作为参考 如与教程不对请参考对应版本

## 一.简易安装办法
1.下载[一键安装包（By PeterJRyan0540）](https://peterdisks540.lanzoum.com/iCeI90kp2yhe)密码：**0540**
[![pPc1gVf.png](https://s1.ax1x.com/2023/09/10/pPc1gVf.png)](https://imgse.com/i/pPc1gVf)
2.双击运行**XiPro Mod和XML地图一键安装工具 By PeterJRyan0540**
3.点击安装即可完成
[![pPc1RIS.png](https://s1.ax1x.com/2023/09/10/pPc1RIS.png)](https://imgse.com/i/pPc1RIS)

## 二.复杂安装办法
以后有时间再更新