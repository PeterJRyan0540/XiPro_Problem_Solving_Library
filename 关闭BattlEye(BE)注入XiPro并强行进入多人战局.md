# 如何关闭BattlEye(Be)注入XiPro并强行进入多人战局

1.打开**Rockstar Games Launcher(R星启动器)**，登录后点击**設定(设置/Settings)**，将**BattlEye**选项取消勾选


![](https://gitea.com/PeterJRyan0540/XiPro_Problem_Solving_Library/raw/branch/main/Tools/1.png)

2.下载  **FSL(version.dll)** 以便于强制进入多人战局
[https://wwd.lanzoum.com/iG3Le2ikdyha](https://wwd.lanzoum.com/iG3Le2ikdyha)

3.下载后将 **FSL(version.dll)** 复制到游戏根目录 根目录位置请参考如下

![](https://gitea.com/PeterJRyan0540/XiPro_Problem_Solving_Library/raw/branch/main/Tools/5.png)



Steam平台用户:打开Steam,点击库，找到Grand Theft Auto V，右键点击**管理-浏览本机目录**

![](https://gitea.com/PeterJRyan0540/XiPro_Problem_Solving_Library/raw/branch/main/Tools/3.png)

Epic平台用户:打开Epic Games Launcher，点击库，右键游戏，点击管理，找到安装右边的图标

![](https://gitea.com/PeterJRyan0540/XiPro_Problem_Solving_Library/raw/branch/main/Tools/2.png)

Rockstar用户和其他平台用户:自行寻找安装根目录

4.进入XiPro，选择 **传统注入** ，即可解决


![](https://gitea.com/PeterJRyan0540/XiPro_Problem_Solving_Library/raw/branch/main/Tools/4.png)
