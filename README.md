# XiPro问题解决库
>这里能看到并解决大部分在**XiPro**中出现的各种问题

>本库内所有文件都遵守[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)许可证，如有经销商或者代理使用本教程需注明作者名为[PeterJRyan0540](https://gitea.com/PeterJRyan0540)

>如果您发现下载下来的教程看不懂上面的文字，那这就对了，因为我使用**Markdown**语言来编写的教程，如有需要可向我索取文字版教程

>注：本库和辅助作者无关系，为本人心血来潮制作

>[XiPro官网](xigta.com) [XiPro最新版下载](https://wwd.lanzoum.com/b02ib4hng)

>如果遇到这里面没有的问题或提出问题，请优先寻找您的**售后**并向其获取解答 然后再点击[这里](https://docs.qq.com/form/page/DRXFRUFZpcVlBdnV1)，而不是在**XiPro用户群**内[询问问题](https://gitea.com/PeterJRyan0540/XiPro_Problem_Solving_Library/src/branch/main/%E5%A6%82%E4%BD%95%E5%90%88%E7%90%86%E7%9A%84%E5%9C%A8%E7%BE%A4%E5%86%85%E6%8F%90%E9%97%AE.md)
[![10790272-190b52301789e31d.jpg](https://upload-images.jianshu.io/upload_images/10790272-190b52301789e31d.jpg)](https://gitea.com/PeterJRyan0540/XiPro_Problem_Solving_Library/src/branch/main/%E5%A6%82%E4%BD%95%E5%90%88%E7%90%86%E7%9A%84%E5%9C%A8%E7%BE%A4%E5%86%85%E6%8F%90%E9%97%AE.md)





[![pP6zgAO.png](https://s1.ax1x.com/2023/09/09/pP6zgAO.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
